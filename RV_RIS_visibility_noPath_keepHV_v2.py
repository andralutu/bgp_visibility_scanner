#!/usr/bin/env python

# this script outputs the number of RTs containing a given prefix at each sampling time
# LV-prefs - prefixes present in less than 95% of the ASes in the sample

import os, time
import sys
from subprocess import Popen, PIPE
import re
import fnmatch
import math

def bytes2bits():
    """Create a table of bit values for int(0) to int(256)."""
    # derived from  Python Cookbook 2e 2005.
    bytes = [None] * 256
    for n in xrange(256):
        bits = []
        q = n                       # q is modified by bitshift
        for b in xrange(0,8):
            if (q & 128):
                bits.append(1)
            else:
                bits.append(0)
            q <<= 1
        bytes[n] = tuple(bits)      # make immutable
    return tuple(bytes)
# One-time call to build table; rebind the function name to its result.
bytes2bits = bytes2bits()

class ProgramError(ValueError):
    """This is a bug"""

class CIDRnode(object):
    def __init__(self, cidr=None):
        self.cidr = cidr
        self.left = None
        self.right = None

    def get_net(self):
        if self.cidr:
            return self.cidr.split('/')[0]
        else:
            return None
    #network = property(get_net, doc='IPv4 dotted quad')

    def get_bits(self):
        if self.cidr:
            return int(self.cidr.split('/')[1])
        else:
            return None
    #netbits = property(get_bits, doc='IPv4 netmask bits')




class CIDRtrie(object):
   
    @staticmethod
    def byte2bit(ip):
        for q in ip:
            q = int(q) 
            for b in xrange(0,8):
                yield 1 if (q & 128) else 0
                q <<= 1

    def add_cidr(self, cidr):
        """Build the trie."""
        c = cidr.split('/')
        if len(c)==2:
            network = c[0].split('.')
            nm_bit = int(c[1])
            classA = int(network[0])   # assuming no network is smaller than a class A /8
            subtree = self.root[classA] 
            if not subtree:
                subtree = CIDRnode()
                self.root[classA] = subtree
            nm_bit -= 7             # leave a bit for test at top of loop
            network_bits = self.byte2bit(network[1:])
            for nextbit in network_bits: 
                if nm_bit == 1:
                    overwrite = subtree.cidr
                    subtree.cidr = cidr
                    return overwrite                # expect to return None
                nm_bit -= 1        
                if nextbit is 0:
                    if not subtree.left:
                        subtree.left = CIDRnode()
                    subtree = subtree.left
                    continue
                elif nextbit is 1:
                    if not subtree.right:
                        subtree.right = CIDRnode()
                    subtree = subtree.right
                    continue
                else:
                    raise ProgramError('Cannot map %s with a least-specific prefix in the table!!!'% cidr)
            #else:
             #    continue
        #else:
         #   continue


    def __init__(self, ipranges=None):
        self.root = [None] * 256        # A forest of (x.0.0.0/8) 'class A' addresses
        if ipranges:
            for cidr in ipranges:
                self.add_cidr(cidr)

    def get_cidr(self, IP):
        """This is very similar to add_cidr but inline code improves
        performance by around 10% and the cost of building the lookup table
        is amortised over serveral million lookups gaining another 10%."""
        ip = IP.split('.')
        subtree = self.root[int(ip[0])]         # subtree = CIDRnode
        if subtree is None:
            return None
        results = [None]
        for quad in ip[1:]:
            quad_bits = bytes2bits[int(quad)]
            for nextbit in quad_bits:
                if subtree.cidr:
                    results.append(subtree.cidr)
                if subtree.left and nextbit is 0:
                    subtree = subtree.left
                    continue
                elif subtree.right and nextbit is 1:
                    subtree = subtree.right
                    continue
                else:
                    return results.pop()


    def get_least_specific_cidr(self, net):
        pref = net.strip().split('/')
        network = pref[0].split('.')
        bitmask = int(pref[1])
        classA = int(network[0])   # assuming no network is smaller than a class A /8
        subtree = self.root[classA]
      
        if subtree is None:
           return None

        results = [None]

        for quad in network[1:]:
            quad_bits = bytes2bits[int(quad)]
            for nextbit in quad_bits:
                if subtree.cidr:
                    results.append(subtree.cidr)
                if subtree.left and nextbit is 0:
                    subtree = subtree.left
                    continue
                elif subtree.right and nextbit is 1:
                    subtree = subtree.right
                    continue
                else:
                    if len(results)>=2:
                     return results[1]
                    else:
                     return None

def get_covers(glob, find):

    covers = dict()

    match_list = []
    row = []
    trie = CIDRtrie()
    overwrite = map(trie.add_cidr, [str(net) for net in glob])

    match = map(trie.get_least_specific_cidr, [str(net) for net in find])
    match_list = list(match)

    for ip, m in zip(find, match_list):
        if ip not in covers:
            covers[ip] = []
            covers[ip].append(m)
        else:
            covers[ip].append(m)
    return covers


def uniq(lst): 
    # unique elements with preserving the order of appearence 
        checked = []
        for e in lst:
            if e not in checked:
                checked.append(e)
        return checked

#TABLE_DUMP2|1291852800|B|4.69.184.193|3356|1.9.0.0/16|3356 1273 4788|IGP|4.69.184.193|0|0|1273:12250 3356:3 3356:22 3356:100 3356:123 3356:575 3356:2006 4788:200 4788:210 65000:0 65000:1239|NAG||

def get_collectors(path):
    # return all the collectors from a certain repository (RIS or RV)
    for path, dirs, files in os.walk(path):
        if len(dirs)>2:
            return dirs

def parse_full_RTs(filename):
        cmd = '/home/andra_lutu/bgpdump/bgpdump -m ' + filename
        proc = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        RTs = dict() # build a dict RT[AS, pref_AS] : (prefix, AS-path)
        while True:
            data = proc.stdout.readline()
            if data == '' and proc.poll() != None: break
            line = data.strip().split('|')
	    if (len(line) < 15): 
		print line
		break
            ASpath = line[6].strip().split(' ')
            path = set(uniq(ASpath))
            origin = ASpath[len(ASpath)-1]
            monitor = line[4] # the AS providing its RT
            m_prefix = line[3] # the prefix for the connected AS to the collector in RIS
            if ":" not in m_prefix:
               pref = line[5]
               if ":" not in str(pref):
                   if (monitor, m_prefix) not in RTs:
                      RTs[monitor, m_prefix] = []
                      RTs[monitor, m_prefix].append((pref, origin))
                   else:
                      RTs[monitor, m_prefix].append((pref, origin))
        full_RTs = dict()
        print "all RTs in the collector: " + str(int(len(RTs)))
        for (monitor, m_prefix) in RTs:
            if len(RTs[monitor, m_prefix]) >400000:
                if (monitor, m_prefix) not in full_RTs:
                    full_RTs[monitor, m_prefix] = RTs[monitor, m_prefix]
        RTs= dict()
        print "full RTs in the collector: " + str(len(full_RTs))
        return full_RTs # returns all the routing tables inlcuded in the <rrc*.gz> from RIS

# here idea would be to eliminate redundant feeds from the same AS and only keep the largest (?) one => one feed per AS
# also, filter only for full RTs (larger than 390.000 entries in the RT)


def collector_analysis(collector_path, stamp, time):
     print "Analyze collector: " + str(collector_path)
     print "STAMP: " + str(stamp)
     collector_full_RTs = dict()
     for path, dirs, files in os.walk(collector_path):
       for filename in files:
          print filename
          if fnmatch.fnmatch(filename, "*.{0}.*".format(unicode(stamp, 'utf-8'))):
              bview, date, t, gz = filename.split(".")
              print "file time: " +  str(t)
              if t == time : 
                  print "Parsing file "  + str(filename) + " ...\n"
                  dump = os.path.abspath(os.path.join(path, filename))
                  collector_full_RTs = parse_full_RTs(dump)
     return collector_full_RTs


risPath = "/home/andra_lutu/external_hd/data.ris.ripe.net"
rvPath = "/home/andra_lutu/external_hd/routeviews.org"

def main():
    print "Apply the new methond of identifying the LV prefixes"
    try:
      print sys.argv
      year = sys.argv[1]
      print "Year:" + str(year)
      month = sys.argv[2]
      print "Month:" + str(month)
      day = sys.argv[3]
      print "DAY:" + str(day)
      outDir= sys.argv[5]
      print "Output dir: " + outDir
      #rtPath = sys.argv[2]
      #print "RTs path: " + rtPath
      bogon_filter_file = sys.argv[4]
      print "Bogon filter used: " + bogon_filter_file
    except:
      print "Input: pref_visibility.py <YEAR> <MONTH> <DAY> <bogon_filter> <output_dir>"
      sys.exit(0)

    current_time = year+month+day
    print "Current time to analyze: " + str(current_time)
    logFile= open(os.path.join(outDir, "log" + str(current_time)), 'w')
    time = [['0800', '0759'], ['1600', '1559']]

    #dictionary of the form: 
    #prefix -> originAS
    originAS = dict()
    #prefix -> RTs which contain the prefix
    ASlist = dict()
    vizibility = dict()
    routing_tables = dict()

    RIS = get_collectors(risPath)
    RV = get_collectors(rvPath)
    #RIS = ['rrc00', 'rrc10']
    #RV = ['route-views.isc', 'route-views.saopaulo']
    print "RIS collectors: " + str(RIS)
    print "RV collector: " + str(RV)
    stamp = year + "." + month
    print "stamp: " + str(stamp)
    all_full_RTs = []
    for i in range(2):
        all_full_RTs.append("")
        all_full_RTs[i] = dict()
        for t in time[i]:
            collector_RTs = dict()
            print "time: " + str(t)
            for collector in RIS: 
              if collector != "test":
                # data.ris.ripe.net/rrc00/2012.10/s
                collector_path = str(risPath) + "/" + str(collector) + "/" + stamp + "/"
                collector_RTs = collector_analysis(collector_path, current_time, t)
                print "# of monitors already identified: " + str(len(all_full_RTs[i]))
                for (monitor, m_prefix) in collector_RTs:
                    if monitor not in all_full_RTs[i]:
                       all_full_RTs[i][monitor] = collector_RTs[monitor, m_prefix]
                    else:
                       continue
                collector_RTs = dict()
            for collector in RV:
              if collector != "bgpdata" :
                # routeviews.org/route-views.isc/bgpdata/2012.10/RIBS/
                collector_path = str(rvPath) + "/" + collector + "/bgpdata/" + stamp + "/RIBS/"
                collector_RTs = collector_analysis(collector_path, current_time, t)
                print "# of monitors already identified: " + str(len(all_full_RTs[i]))
                for (monitor, m_prefix) in collector_RTs:
                    if monitor not in all_full_RTs[i]:
                       all_full_RTs[i][monitor] = collector_RTs[monitor, m_prefix]
                    else:
                       continue
                collector_RTs = dict()
              else:
                collector_path = str(rvPath) + "/bgpdata/" + stamp + "/RIBS/"
                collector_RTs = collector_analysis(collector_path, current_time, t)
                print "# of monitors already identified: " + str(len(all_full_RTs[i]))
                for (monitor, m_prefix) in collector_RTs:
                    if monitor not in all_full_RTs[i]:
                       all_full_RTs[i][monitor] = collector_RTs[monitor, m_prefix]
                    else:
                       continue
                collector_RTs = dict()

# at this point we have unique information from all the monitors dumping FULL routing tables in all_full_RTs for each of the two moments of interest
# now we have to apply the filters


    all_monitors = []
    ASlist = []
    originAS = []

    for i in range(2):
        all_monitors.append("")
        originAS.append("")
        ASlist.append("")
        originAS[i] = dict()
        ASlist[i] = dict()
        all_monitors[i] = set()

        for rt in all_full_RTs[i]:
            all_monitors[i].add(rt)
            for (prefix, origin) in all_full_RTs[i][rt]:
                if prefix not in ASlist[i]:
                    ASlist[i][prefix] = set()
                    ASlist[i][prefix].add(rt)
                    originAS[i][prefix] = set()
                    originAS[i][prefix].add(origin)
    	        else: 
                    ASlist[i][prefix].add(rt)
                    originAS[i][prefix].add(origin)

 
    for i in range(2):
        print "Number of discovered prefixes:" + str(len(ASlist[0])) +  " at time t1 and " + str(len(ASlist[1])) + " at moment t2.\n" 
        logFile.write("Total number of discovered prefixes: " + str(len(ASlist[0])) + " at moment t1 and " + str(len(ASlist[1])) + " at moment t2.\n")
    monitors = []
      # check the visibility of the prefixes and output the ones with high and limited visibility and their origin ASes
    if set(all_monitors[1]) == set(all_monitors[0]):
       print "Same monitors at both times!!!\n"
       full_visibility = len(all_monitors[1])
       monitors = all_monitors[0]

    else:
       print "Not the same number of monitors in both samples!"
       dif_monitors = set(list(set(all_monitors[1]) - set(all_monitors[0])) + list(set(all_monitors[0]) - set(all_monitors[1])))
       print "Unique ASes: " + str(dif_monitors)
       monitors = set(list(set(all_monitors[1])) + list(set(all_monitors[0]))) - set(dif_monitors)

    limVis = []
    for i in range(2):
       viz = math.floor(0.95 * int(len(all_monitors[i]))) 
       limVis.append(viz)

    print "Number of monitors: " + str(len(all_monitors[0])) + " " + str(len(all_monitors[1]))
    print str(len(monitors)) + " monitors used: " + str(monitors)
    print "Visibility limit: " + str(limVis)
    logFile.write("Visibility limit: " + str(limVis) + "\n") 

    filterFilename = os.path.join(outDir, "filtered_prefs_" + str(current_time))
    filterFile = open(filterFilename, 'w')
    LVprefsFilename = os.path.join(outDir, "LVprefs_" + str(current_time))
    HVprefsFilename = os.path.join(outDir, "HVprefs_" + str(current_time))


    lv_prefs = []
    hv_prefs = []

    oneSample_prefs = set(list(set(set(ASlist[0]) - set(ASlist[1]))) + list(set(ASlist[1]) - set(ASlist[0])))
    for prefix in oneSample_prefs:
        if prefix in ASlist[0]:
           if len(ASlist[0][prefix]) > limVis[0]:
               hv_prefs.append(prefix)
           else:
               filterFile.write("LV prefix not in both time samples: " + str(prefix) + "\n")  
        else:
            if len(ASlist[1][prefix]) > limVis[1]:
                hv_prefs.append(prefix)
            else:
                filterFile.write("LV prefix not in both time samples: " + str(prefix) + "\n")  

    LVprefsFile = open(LVprefsFilename, 'w')
    LVprefsFile.write("# prefixes with limited visibility in the sample \n")
    LVprefsFile.write("# <origin-AS> <prefix> <visibility> <AS-list> \n")

    HVprefsFile = open(HVprefsFilename, 'w')
    HVprefsFile.write("# prefixes with high visibility in the sample \n")
    HVprefsFile.write("# <origin-AS> <prefix> <visibility> \n")


      
    const_prefs = set(list(set(ASlist[0])) + list(set(ASlist[1]))) - set(oneSample_prefs)
    for prefix in const_prefs:
        if len(ASlist[1][prefix]) <= limVis[1] and len(ASlist[0][prefix]) <= limVis[0]: 
            lv_prefs.append(prefix)
        elif len(ASlist[0][prefix]) > limVis[0] or len(ASlist[1][prefix]) > limVis[1]:
            hv_prefs.append(prefix)


    print "LV_prefs: " + str(len(lv_prefs))
    print "HV_prefs: " + str(len(hv_prefs))
    bogon_filter = []
    bogons = open(bogon_filter_file, 'r')
    for row in bogons:  
        bogon_filter.append(row.strip())

    lv_covers = get_covers(bogon_filter, lv_prefs)
    hv_covers = get_covers(bogon_filter, hv_prefs)
    LVprefs = []
    HVprefs = []

    for ip in lv_covers:
        #print str(originAS[0][ip]) + " " + str(originAS[1][ip])
        #print str(ip) + " " + str(lv_covers[ip])
        if None in lv_covers[ip]:
            if len(originAS[0][ip])==len(originAS[1][ip]) and len(originAS[0][ip])==1 :
              if set(originAS[0][ip])!=set(ASlist[0][ip]):
                LVprefs.append(ip)
              else:
                filterFile.write("Internal: " + str(ip) +  "|" + str(originAS[0][ip])  + "|" + str(ASlist[0][ip]) + "\n")
            else:
                filterFile.write("MOAS: " + str(ip) +  "|" + str(originAS[0][ip])  + "|" + str(ASlist[0][ip]) + "\n")
        else:
            filterFile.write("Bogon: " + str(ip) + "|" + str(lv_covers[ip]) + "|" + str(ASlist[0][ip]) + "\n")


    for ip in hv_covers:
      if ip in ASlist[0] and ip in ASlist[1]:
        if None in hv_covers[ip]:
            if len(originAS[0][ip]) == len(originAS[1][ip]) and len(originAS[0][ip])==1:
                HVprefs.append(ip)
                HVprefsFile.write(str(list(set(originAS[1][ip]))) + " " + str(ip) + " " + str(len(ASlist[1][ip])) + " " + str(list(ASlist[1][ip])) + "\n")
            else:
                filterFile.write("MOAS: " + str(ip) +  "|" + str(originAS[0][ip])  + "|" + str(ASlist[0][ip]) + "\n")
        else:
            filterFile.write("Bogon: " + str(ip) + "|" + str(hv_covers[ip]) + "|" + str(list(ASlist[0][ip])) + "\n")
      else:
        if ip in ASlist[0]:
          if None in hv_covers[ip]:
            if len(originAS[0][ip])==1:
                HVprefs.append(ip)
                HVprefsFile.write(str(" ".join(list(set(originAS[0][ip])))) + " " + str(ip) + " " + str(len(ASlist[0][ip])) + " " + str(" ".join(list(ASlist[0][ip]))) + "\n")
            else:
                filterFile.write("MOAS: " + str(ip) +  "|" + str(originAS[0][ip])  + "|" + str(ASlist[0][ip]) + "\n")
          else:
            filterFile.write("Bogon: " + str(ip) + "|" + str(hv_covers[ip]) + "|" + str(list(ASlist[0][ip])) + "\n")
        else:
          if None in hv_covers[ip]:
            if len(originAS[1][ip])==1:
                HVprefs.append(ip)
                HVprefsFile.write(str(list(set(originAS[1][ip]))) + " " + str(ip) + " " + str(len(ASlist[1][ip])) + " " + str(list(ASlist[1][ip])) + "\n")
            else:
                filterFile.write("MOAS: " + str(ip) +  "|" + str(originAS[1][ip])  + "|" + str(ASlist[1][ip]) + "\n")
          else:
            filterFile.write("Bogon: " + str(ip) + "|" + str(hv_covers[ip]) + "|" + str(list(ASlist[1][ip])) + "\n")  


    #identify the dark perfixes and put a marker in the output

    logFile.write("Number of LV prefixes: " + str(len(LVprefs)) + "\n")
    logFile.write("Number of HV prefixes: " + str(len(HVprefs)) + "\n")
    print "Number of LV prefixes after filtering: " + str(len(LVprefs)) + "\n"
    print "Number of HV prefixes after filtering: " + str(len(HVprefs)) + "\n"

    DP_prefs = get_covers(HVprefs, LVprefs)
    for ip in DP_prefs:
        if None in DP_prefs[ip]:
           LVprefsFile.write("DP|" + str(" ".join(list(set(originAS[0][ip])))) + "|" + str(ip) + "|" + str(len(ASlist[0][ip])) + "|" +str(" ".join(list(ASlist[0][ip]))) + "|" + str(" ".join(list(set(monitors) - set(ASlist[0][ip])))) + "\n")
        else:
           LVprefsFile.write("LV|" + str(" ".join(list(set(originAS[0][ip])))) + "|" + str(ip) + "|" + str(len(ASlist[0][ip])) + "|" +str(" ".join(list(ASlist[0][ip]))) + "|" + str(" ".join(list(set(monitors) - set(ASlist[0][ip])))) + "\n")


    unstableLVFilename = os.path.join(outDir, "unstable_LVprefs_" + str(current_time))
    stableLVFilename = os.path.join(outDir, "stable_LVprefs_" + str(current_time))
    unstable_file = open(unstableLVFilename, 'w')
    stable_file = open(stableLVFilename, 'w')
    stable_lv_prefs = []
    unstable_lv_prefs = []


    for prefix in LVprefs:
               if ASlist[1][prefix] == ASlist[0][prefix]:
                  stable_lv_prefs.append(prefix)
                  stable_file.write(str(prefix) +  "|" + str(ASlist[0][prefix]) +"\n")
               else:
                  unstable_lv_prefs.append(prefix) ### AHA!!!
                  unstable_file.write(str(prefix) + "|" + str(ASlist[0][prefix]) + "|" + str(ASlist[1][prefix]) + "\n" )
     
    LVprefsFile.close()
    HVprefsFile.close()
    filterFile.close()
    logFile.close()
    unstable_file.close()
    stable_file.close()


if __name__ == '__main__':     #only when run from cmd line
    start = time.time()
    main()
    t = time.time()-start
    print 'execution lasted %s ' % t

