#!/usr/bin/env python

# this script returns the relationship type between the ASes in the first link (originAS - nextAS) for the prefixes identified as LV

import os, time
import sys
from subprocess import Popen, PIPE
import re
import fnmatch
import math
import gzip 

def uniq(lst): 
    # unique elements with preserving the order of appearence 
        checked = []
        for e in lst:
            if e not in checked:
                checked.append(e)
        return checked

#TABLE_DUMP2|1291852800|B|4.69.184.193|3356|1.9.0.0/16|3356 1273 4788|IGP|4.69.184.193|0|0|1273:12250 3356:3 3356:22 3356:100 3356:123 3356:575 3356:2006 4788:200 4788:210 65000:0 65000:1239|NAG||

def get_collectors(path):
    # return all the collectors[rrc<> route_views.<>] from a certain repository (RIS or RV)
    for path, dirs, files in os.walk(path):
        if len(dirs)>2:
            return dirs

def parse_full_RTs(filename):
        cmd = '/srv/alutu/bgpdump/bgpdump -m ' + filename
        proc = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        RTs = dict() # build a dict RT[AS, pref_AS] : (prefix, AS-path)
        while True:
            data = proc.stdout.readline()
            if data == '' and proc.poll() != None: break
            line = data.strip().split('|')
	    if (len(line) < 15): 
		print line
		break
            ASpath = line[6].strip().split(' ')
            path = uniq(ASpath)
            monitor = line[4] # the AS providing its RT
            m_prefix = line[3] # the prefix for the connected AS to the collector in RIS
            if ":" not in m_prefix:
               pref = line[5]
               if ":" not in str(pref):
                   if (monitor, m_prefix) not in RTs:
                      RTs[monitor, m_prefix] = dict()
                      RTs[monitor, m_prefix][pref] = path
                   else:
                      RTs[monitor, m_prefix][pref] = path
        full_RTs = dict()
        for (monitor, m_prefix) in RTs:
            if len(RTs[monitor, m_prefix]) > 400000:
                if (monitor, m_prefix) not in full_RTs:
                    full_RTs[monitor, m_prefix] = RTs[monitor, m_prefix]
        RTs= dict()
        return full_RTs # returns all the routing tables inlcuded in the <rrc*.gz> from RIS

# here idea would be to eliminate redundant feeds from the same AS and only keep the largest (?) one => one feed per AS
# also, filter only for full RTs (larger than 390.000 entries in the RT)


def collector_analysis(collector_path, stamp, time):
     print "Analyze collector: " + str(collector_path)
     collector_full_RTs = dict()
     for path, dirs, files in os.walk(collector_path):
       for filename in files:
          if fnmatch.fnmatch(filename, "*.{0}.*".format(unicode(stamp, 'utf-8'))):
           check = filename.split(".")
           if len(check) == 4:
              bview, date, t, gz = filename.split(".")
              if t == time : 
                  print "Parsing file "  + str(filename) + " ...\n"
                  dump = os.path.abspath(os.path.join(path, filename))
                  collector_full_RTs = parse_full_RTs(dump)
           else:
              continue
     return collector_full_RTs


risPath = "/srv/alutu/BGP_scanner/data.ris.ripe.net"
rvPath = "/srv/alutu/BGP_scanner/routeviews.org"

def main():
    print "Retrieve the type of link between the first two ASes in the AS-Path	"
    try:
      print sys.argv
      year = sys.argv[1]
      print "Year:" + str(year)
      month = sys.argv[2]
      print "Month:" + str(month)
      day = sys.argv[3]
      print "Day:" + str(day)
      outDir= sys.argv[4]
      print "Output dir: " + outDir
    except:
      print "Input: get_AS-Path.py <YEAR> <MONTH> <DAY> <output_dir>"
      sys.exit(0)

    current_time = year+month+day
    stamp = year + "." + month
    print "Current time to analyze: " + str(current_time)
    #logFile= open(os.path.join(outDir, "log" + str(current_time)), 'w')
    time = ['0800', '0759']
    LVoutputFilename = os.path.join(outDir, "AS-Path_LV_" + current_time)
    LVoutFile = open(LVoutputFilename, 'w')
    HVoutputFilename = os.path.join(outDir, "AS-Path_HV_" + current_time)
    HVoutFile = open(HVoutputFilename, 'w')

# load the pre-determined list of LV prefixes
    LVprefsFilename = os.path.join("/srv/alutu/BGP_scanner/PREFS/" +str(stamp)+ "/LVprefs" , "LVprefs_" + str(current_time))
    LVprefsFile = open(LVprefsFilename, 'r')
    lv_prefs = []
    for line in LVprefsFile:
        lv_prefs.append(line.strip())

# load the pre-determined list of HV prefixes
    HVprefsFilename = os.path.join("/srv/alutu/BGP_scanner/PREFS/" +str(stamp)+ "/HVprefs" , "HVprefs_" + str(current_time))
    HVprefsFile = open(HVprefsFilename, 'r')
    hv_prefs = []
    for line in HVprefsFile:
        hv_prefs.append(line.strip())

    RIS = get_collectors(risPath)
    RV = get_collectors(rvPath)
    #RIS = ['rrc00', 'rrc10']
    #RV = ['route-views.isc', 'route-views.saopaulo']
    print "RIS collectors: " + str(RIS)
    print "RV collector: " + str(RV)

    #print "stamp: " + str(stamp)
    path_LVprefs = dict()
    path_HVprefs = dict()
    all_full_RTs = dict() # keep track of the monitors that we select for our study : collector and ip address
    #all_monitors = dict()
    for t in time:
            collector_RTs = dict()
            print "time: " + str(t)
            for collector in RIS: 
              if collector != "test":
                # data.ris.ripe.net/rrc00/2012.10/s
                collector_path = str(risPath) + "/" + str(collector) + "/" + stamp + "/"
                collector_RTs = collector_analysis(collector_path, current_time, t) # all the full routing tables dumped in the collector being analyzed
                print "# of monitors already identified: " + str(len(all_full_RTs))
                for (monitor, m_prefix) in collector_RTs:
                    print "Monitor: " + str(monitor)
                    if monitor not in all_full_RTs:
                       all_full_RTs[monitor] = []
                       all_full_RTs[monitor].append((collector, m_prefix))
                       rt_prefs = [p for p in collector_RTs[monitor, m_prefix]] # get all the prefixes from the full RT (monitor, m_prefix)
                       rt_lv_prefs = set(lv_prefs) - set(set(lv_prefs) - set(rt_prefs)) # these are the LV prefs within the RT we analyze
                       rt_hv_prefs = set(hv_prefs) - set(set(hv_prefs) - set(rt_prefs)) # these are the HV prefs within the RT we analyze
                       print "Prefixes in monitor " + str(monitor) + ": " + str(len(rt_prefs))
                       print "LVprefs in monitor " + str(monitor) + ": " + str(len(rt_lv_prefs)) 
                       print "HVprefs in monitor " + str(monitor) + ": " + str(len(rt_hv_prefs)) 

                       path_LVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_lv_prefs} # create a new dict which contains only the LVprefs
                       path_HVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_hv_prefs}
                    else:
                       continue
                collector_RTs = dict()
              else:
                  continue 

            for collector in RV:
              if collector != "bgpdata" :
                # routeviews.org/route-views.isc/bgpdata/2012.10/RIBS/
                collector_path = str(rvPath) + "/" + collector + "/bgpdata/" + stamp + "/RIBS/"
                collector_RTs = collector_analysis(collector_path, current_time, t)
                print "# of monitors already identified: " + str(len(all_full_RTs))
                for (monitor, m_prefix) in collector_RTs:
                    print "Monitor: " + str(monitor)
                    if monitor not in all_full_RTs:
                       all_full_RTs[monitor] = []
                       all_full_RTs[monitor].append((collector, m_prefix))
                       rt_prefs = [p for p in collector_RTs[monitor, m_prefix]] # get all the prefixes from the full RT (monitor, m_prefix)
                       rt_lv_prefs = set(lv_prefs) - set(set(lv_prefs) - set(rt_prefs)) # these are the LV prefs within the RT we analyze
                       rt_hv_prefs = set(hv_prefs) - set(set(hv_prefs) - set(rt_prefs)) # these are the HV prefs within the RT we analyze
                       print "LVprefs in monitor " + str(monitor) + ": " + str(len(rt_lv_prefs)) 
                       print "HVprefs in monitor " + str(monitor) + ": " + str(len(rt_hv_prefs)) 

                       path_LVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_lv_prefs} # create a new dict which contains only the LVprefs
                       path_HVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_hv_prefs}
                    else:
                       continue
                collector_RTs = dict()
              else:
                collector_path = str(rvPath) + "/bgpdata/" + stamp + "/RIBS/"
                collector_RTs = collector_analysis(collector_path, current_time, t)
                print "# of monitors already identified: " + str(len(all_full_RTs))
                for (monitor, m_prefix) in collector_RTs:
                    print "Monitor: " + str(monitor)
                    if monitor not in all_full_RTs:
                       all_full_RTs[monitor] = []
                       all_full_RTs[monitor].append((collector, m_prefix))
                       rt_prefs = [p for p in collector_RTs[monitor, m_prefix]] # get all the prefixes from the full RT (monitor, m_prefix)
                       rt_lv_prefs = set(lv_prefs) - set(set(lv_prefs) - set(rt_prefs)) # these are the LV prefs within the RT we analyze
                       rt_hv_prefs = set(hv_prefs) - set(set(hv_prefs) - set(rt_prefs)) # these are the HV prefs within the RT we analyze
                       print "LVprefs in monitor " + str(monitor) + ": " + str(len(rt_lv_prefs)) 
                       print "HVprefs in monitor " + str(monitor) + ": " + str(len(rt_hv_prefs)) 

                       path_LVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_lv_prefs} # create a new dict which contains only the LVprefs
                       path_HVprefs[monitor] = {pref : collector_RTs[monitor, m_prefix][pref] for pref in rt_hv_prefs}
                    else:
                       continue
                collector_RTs = dict()

# at this point we have all the full LV prefs in the monitors and their type of first link
    print all_full_RTs

    for monitor in path_LVprefs:
        for prefix in path_LVprefs[monitor]:
            LVoutFile.write(str(prefix) + "|" + str(monitor) + "|" + str(' '.join(path_LVprefs[monitor][prefix])) + "\n")

    for monitor in path_HVprefs:
        for prefix in path_HVprefs[monitor]:
            HVoutFile.write(str(prefix) + "|" + str(monitor) + "|" + str(' '.join(path_HVprefs[monitor][prefix])) + "\n")

# until here we've got the AS-paths for the already determined LV and HV prefixes
# now get the geo-spread of all the ASes in the path
# merge all the paths and then merge all the countries (?)


        


    

if __name__ == '__main__':     #only when run from cmd line
    start = time.time()
    main()
    t = time.time()-start
    print 'execution lasted %s ' % t

