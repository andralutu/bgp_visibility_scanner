#!/bin/sh

# run the script on a per month basis -- to retrieve all the RTs in a month

for url in $(cat ../RIS_urls); do  nohup wget -e robots=off -r -l1 -np  -A "bview."$(echo $1$2)"*" $url"/"$( echo $1.$2)"/" & done;
for url in $(cat ../route_views_bgpdata); do nohup wget -e robots=off -r -l1 -np  -A "rib."$(echo $1$2)"*" "http://routeviews.org/"$url"/"$( echo $1.$2)"/RIBS/" & done;
