#!/usr/bin/env python

"""
Create a digital trie for the CIDRs in cidr_file: for a given network net
find the least-specific matching prefix in the trie
"""
import pp
import sys, time 
from netaddr import *


__STATE = {'server': None}



def ppmap(processes, function, sequence, *sequences):
# basic parallel processing of a given function
    """Split the work of 'function' across the given number of
    processes.  Set 'processes' to None to let Parallel Python
    autodetect the number of children to use.
    """

    # Create a new server if one isn't already initialized
    if not __STATE['server']:
        __STATE['server'] = pp.Server()
    
    def submit(*args):
        """Send a job to the server"""
        return __STATE['server'].submit(function, args, globals=globals())

    # Merge all the passed-in argument lists together.  This is done
    # that way because as with the map() function, at least one list
    # is required but the rest are optional.
    a = [sequence]
    a.extend(sequences)

    # Set the requested level of multi-processing
    __STATE['server'].set_ncpus(processes or 'autodetect')

    # First, submit all the jobs.  Then harvest the results as they
    # come available.
    return (subproc() for subproc in map(submit, *a))

def bytes2bits():
    """Create a table of bit values for int(0) to int(256)."""
    # derived from  Python Cookbook 2e 2005.
    bytes = [None] * 256
    for n in xrange(256):
        bits = []
        q = n                       # q is modified by bitshift
        for b in xrange(0,8):
            if (q & 128):
                bits.append(1)
            else:
                bits.append(0)
            q <<= 1
        bytes[n] = tuple(bits)      # make immutable
    return tuple(bytes)
# One-time call to build table; rebind the function name to its result.
bytes2bits = bytes2bits()

class ProgramError(ValueError):
    """This is a bug"""

class CIDRnode(object):
    def __init__(self, cidr=None):
        self.cidr = cidr
        self.left = None
        self.right = None

    def get_net(self):
        if self.cidr:
            return self.cidr.split('/')[0]
        else:
            return None
    #network = property(get_net, doc='IPv4 dotted quad')

    def get_bits(self):
        if self.cidr:
            return int(self.cidr.split('/')[1])
        else:
            return None
    #netbits = property(get_bits, doc='IPv4 netmask bits')



class CIDRtrie(object):
   
    @staticmethod
    def byte2bit(ip):
        for q in ip:
            q = int(q) 
            for b in xrange(0,8):
                yield 1 if (q & 128) else 0
                q <<= 1

    def add_cidr(self, cidr):
        """Build the trie."""
        c = cidr.split('/')
        network = c[0].split('.')
        nm_bit = int(c[1])
        classA = int(network[0])   # assuming no network is smaller than a class A /8
        subtree = self.root[classA] 
        if not subtree:
            subtree = CIDRnode()
            self.root[classA] = subtree
        nm_bit -= 7             # leave a bit for test at top of loop
        network_bits = self.byte2bit(network[1:])
        for nextbit in network_bits: 
            if nm_bit == 1:
                overwrite = subtree.cidr
                subtree.cidr = cidr
                return overwrite                # expect to return None
            nm_bit -= 1        
            if nextbit is 0:
                if not subtree.left:
                    subtree.left = CIDRnode()
                subtree = subtree.left
                continue
            elif nextbit is 1:
                if not subtree.right:
                    subtree.right = CIDRnode()
                subtree = subtree.right
                continue
            else:
                raise ProgramError(
                    'Cannot map %s with a least-specific prefix in the table!!!'% cidr)


    def __init__(self, ipranges=None):
        self.root = [None] * 256        # A forest of (x.0.0.0/8) 'class A' addresses
        if ipranges:
            for cidr in ipranges:
                self.add_cidr(cidr)

    def get_least_specific_cidr(self, net):
        pref = net.split('/')
        network = pref[0].split('.')
        bitmask = int(pref[1])
        classA = int(network[0])   # assuming no network is smaller than a class A /8
        subtree = self.root[classA]
      

        if subtree is None:
           return None

        results = []

        for quad in network[1:]:
            quad_bits = bytes2bits[int(quad)]
            for nextbit in quad_bits:
                if subtree.cidr:
                    results.append(subtree.cidr)
                if subtree.left and nextbit is 0:
                    subtree = subtree.left
                    continue
                elif subtree.right and nextbit is 1:
                    subtree = subtree.right
                    continue
                else:   
                    if len(results) >1:
                      return str(results[0])
                    else:
                      return None
def test():
    networks = []
    search_net = []
    f = open('file2_test', 'r+')
    glob = open('file1_test', 'r+')
    for row in glob: 
       networks.append(row)
    for row in f: 
       search_net.append(row)

    
    trie = CIDRtrie()
    for cidr in networks:
        overwrite = trie.add_cidr(cidr)
       

    for net in search_net:
        cidr = trie.get_least_specific_cidr(net)
        if cidr:
            print('Prefix %s is covered by the least-specific prefix %s'% (net, cidr))
        else:
            print('Covering prefix not found for %s'% net)

def main():
    networks = []
    search_net = []
    find = open('10perc_62RTs', 'r+')
    glob = open('90perc_62RTs', 'r+')
    fl_rez = open('10_90_matching_results', 'w')
    for row in glob: 
       x = IPNetwork(row)
       networks.append(row)
    for row in find:
       y = IPNetwork(row)
       search_net.append(row)
    match_list = []
    row = []
    trie = CIDRtrie()
    overwrite = map(trie.add_cidr, [str(net) for net in networks])
    #for cidr in networks:
    #    overwrite = trie.add_cidr(cidr)
    match = map(trie.get_least_specific_cidr, [str(net) for net in search_net])
    match_list = list(match)

    for ip, m in zip(search_net, match_list):
       row = [str(ip).rstrip('\n'), str(m).rstrip('\n')]
       fl_rez.write(str(row))
       fl_rez.write('\n')
       row = []


if __name__ == '__main__':     #only when run from cmd line
    start = time.time()
    main()
    t = time.time()-start
    print 'execution lasted %s ' % t

