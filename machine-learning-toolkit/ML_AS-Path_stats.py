# coding: utf-8
#!/usr/bin/env python
from __future__ import division

#import scipy
import numpypy
import os, time
import sys
from subprocess import Popen, PIPE
import re
import fnmatch
import math
import datetime
import numpy


def uniq(lst): 
    # unique elements with preserving the order of appearence 
        checked = []
        for e in lst:
            if e not in checked:
                checked.append(e)
        return checked


def read_sample(sample_filename):

# one line in the sample file:
#day|pref|orgAS|mean_AS-PAth
#20121027|187.103.97.0/24|14840|1.91667
    sample_file = open(sample_filename, 'r')
    sample = []
    for line in sample_file:
        data = line.strip().split("|")
        sample.append(data)
    return sample

def main():
    try:
        print sys.argv
	
        samplef = sys.argv[1]
        print "Sample filename: " + str(samplef)

        outDir = sys.argv[2]
        print "Output directory: " + str(outDir)
    except:
        print "Input: modify_sample.py <sample_filename> <output_dir>"
        sys.exit(0)

    input_sample = read_sample(samplef)


    path_lengths = dict()
    for line in input_sample:
        pref = line[1]
        day = line[0]
        orgAS = line[2]
        asPath = line[3]
        if (pref, orgAS) not in path_lengths:
           path_lengths[pref, orgAS] = []
           path_lengths[pref, orgAS].append(float(asPath))
        else:
           path_lengths[pref, orgAS].append(float(asPath))


# output format: pref|orgAS|mean_AS-Path|std_AS-Path|max_AS-Path|min_AS-Path
    for (pref, org) in path_lengths:
        paths = path_lengths[pref, org]
        #print str(paths)
        pt = numpy.array(paths)
        mean = numpy.mean(pt)
        std = numpy.std(pt)
        print str(pref) + "|" + str(org) + "|" + str(mean) + "|" + str(std) + "|" + str(max(paths)) + "|" +  str(min(paths))

if __name__ == '__main__':     #only when run from cmd line

    main()


